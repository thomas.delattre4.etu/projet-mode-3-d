package plyParser;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Classe contenant les fonctions générales nécessaires pour l'analyse de chaine
 */
public class ParserHelper {
    private final PlyReader plyReader;
    private final static NumberFormat doubleParser = NumberFormat.getInstance(Locale.US);

    public ParserHelper(PlyReader plyReader) {
        this.plyReader = plyReader;
    }

    /**
     * Recherche dans une chaine.
     * @param pattern pattern (compilé) à chercher
     * @param line chercher dans cette ligne
     * @return la chaine trouvée
     */
    String find(Pattern pattern, String line) {
        Matcher matcher = pattern.matcher(line);

        if (matcher.find()) {
            return matcher.group();
        } else throw WrongFileFormatException.errorAtLine(line, plyReader.lineNumber());
    }

    /**
     * Recherche toute les occurences dans une chaine.
     * @param pattern pattern à chercher
     * @param line chercher dans cette ligne
     * @return un tableau des occurences trouvées
     */
    ArrayList<String> findAll(Pattern pattern, String line) {
        Matcher matcher = pattern.matcher(line);

        ArrayList<String> matched = new ArrayList<>();
        while(matcher.find()) {
            matched.add(matcher.group());
        }

        if(matched.isEmpty()) throw WrongFileFormatException.errorAtLine(line, plyReader.lineNumber());

        return matched;
    }

    /**
     * Parse un Integer.
     * @param number l'Integer à convertir
     * @return Integer
     */
    int parseInt(String number) {
        try {
            return Integer.parseInt(number);
        } catch (NumberFormatException e) {
            throw WrongFileFormatException.numberParsingError(number, plyReader.lineNumber());
        }
    }

    /**
     * Parse un Double.
     * @param number le Double à convertir
     * @return Double
     */
    double parseDouble(String number) {
        try {
        	return doubleParser.parse(number.toUpperCase()).doubleValue();
        } catch (ParseException e) {
            throw WrongFileFormatException.numberParsingError(number, plyReader.lineNumber());
        }
    }
}
