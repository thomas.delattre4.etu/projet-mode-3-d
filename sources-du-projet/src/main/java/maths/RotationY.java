package maths;

import model.Vertex;

/**
 * Calcul de la rotation par rapport a l'axe Y
 * @author clotaire.dufresne.etu
 *
 */
public class RotationY extends TransformationSimple {
	
	/**
	 * Permet de stocker la valeur qui sera utilisee au calcul
	 * @param angle angle en radian
	 */
	public RotationY(Object angle) {
		super(angle);
	}

	@Override
	public void calculate(Vertex v) {
		double radian = (double) super.value;
		
		double xValue = Math.cos(radian)*v.getX() + ((-1.0)*Math.sin(radian))*v.getZ();
		double yValue = v.getY();
		double zValue = Math.sin(radian)*v.getX() + Math.cos(radian)*v.getZ();
		
		super.setVertexNewValues(v, xValue, yValue, zValue);
	}
	
	@Override
	public void calculateVNU(Vertex vnu) {
		this.calculate(vnu);
	}

	@Override
	public String toString() {
		return "RotationY{" +
				"angle=" + value +
				'}';
	}
}