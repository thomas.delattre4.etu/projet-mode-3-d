package controller;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.Observable;
import java.util.Observer;

import javafx.beans.property.BooleanProperty;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import model.Figure;
import model.Polygon;
import util.BenchmarkHelper;

/**
 * Controller du canvas
 * @author adil.benameur.etu
 *
 */
public class CanvasController implements Observer {
	private final int RGB_MAX = 255;
	private Canvas canvas;
	private GraphicsContext gc;
	
	private BooleanProperty shadow;
	private BooleanProperty strokes;
	private BooleanProperty faces;

	private Property<Color> fillColor = new SimpleObjectProperty<>(Color.WHITE);
	private Property<Color> strokeColor = new SimpleObjectProperty<>(Color.BLUEVIOLET);
	private MouseDraggedListener mdl;
	private FigureController figureController;

	public CanvasController(Canvas canvas, FigureController figureController, BooleanProperty shadow, BooleanProperty strokes, BooleanProperty faces, boolean editable) {
		this.canvas = canvas;
		this.shadow = shadow;
		this.strokes = strokes;
		this.faces = faces;
		this.figureController = figureController;

		figureController.normalize(canvas.getWidth(), canvas.getHeight());
		figureController.centrage(canvas.getWidth(), canvas.getHeight());
		figureController.addObserver(this);

		this.gc = canvas.getGraphicsContext2D();
		gc.setStroke(strokeColor.getValue());
		strokeColor.addListener((observable, oldValue, newValue) -> {
			gc.setStroke(newValue);
			reloadCanvas();
		});
		gc.setFill(fillColor.getValue());
		fillColor.addListener((observable, oldValue, newValue) -> {
			gc.setFill(newValue);
			reloadCanvas();
		});

		if(editable) {
			// rotation
			this.mdl = new MouseDraggedListener(figureController);

			canvas.setOnMouseDragged(mdl);

			canvas.setOnMouseReleased(e -> {
				mdl.setDragMousePosition(null);
			});

			// zoom
			canvas.setOnScroll(new MouseScrolledListener(figureController));
		}

		setCanvasResizeListener(figureController);

		shadow.addListener(observable -> {reloadCanvas();});
		faces.addListener(observable -> {reloadCanvas();});
		strokes.addListener(observable -> {reloadCanvas();});

		clearCanvas();
		drawFigure();
	}

	/**
	 * definit le canvas sur lequel tout s'affichera en affectant les differentes actions qui peuvent etre faites dessus
	 * @param canvas canvas qui sera utilise pour l'affichage
	 */
	public CanvasController(Canvas canvas, FigureController figureController, boolean editable) {
		this(canvas, figureController, new SimpleBooleanProperty(true), new SimpleBooleanProperty(false), new SimpleBooleanProperty(true), editable);
	}

	/**
	 * Configure les listners necessaires au rechargement la figure quand la taille du canvas est modifiee
	 */
	private void setCanvasResizeListener(FigureController figureController) {
		ChangeListener<Number> resize = (observableValue, oldNumber, newNumber) -> {
			figureController.normalize(canvas.getWidth(), canvas.getHeight());
			figureController.centrage(canvas.getWidth(), canvas.getHeight());
		};

		canvas.heightProperty().addListener(resize);
		canvas.widthProperty().addListener(resize);
	}

	public Property<Color> getFillColor() {
		return fillColor;
	}

	public Property<Color> getStrokeColor() {
		return strokeColor;
	}

	/**
	 * nettoie le canvas puis raffiche la figure
	 */
	public void reloadCanvas() {
		System.out.println(strokes.get());
		clearCanvas();
		drawFigure();
	}

	private void drawFigure() {
		BenchmarkHelper benchmarkHelper = new BenchmarkHelper();
		System.out.println("Drawing " + figureController.numberOfPolygons() + " polygons");
		figureController.forEachPolygons(p -> {
			double[] xValues = p.getX();
			double[] yValues = p.getY();
			for(int i = 0; i < yValues.length; i++) {
				yValues[i] = canvas.getHeight() - yValues[i];
			}

			if(shadow.get())
				shadowSetup(p);

			if (faces.get())
				gc.fillPolygon(xValues, yValues, p.getVerticesNumber());

			if (strokes.get())
				gc.strokePolygon(xValues, yValues, p.getVerticesNumber());
		});

		System.out.println("Took " + benchmarkHelper.getSeconds() + " seconds (" + (benchmarkHelper.getMilliSeconds()/figureController.numberOfPolygons()) + " ms for one poylgon)");
	}


	/**
	 * determine l'ombrage sur le polygone p
	 * @param p le polygone sur lequel sera applique l'ombrage
	 */
	private void shadowSetup(Polygon p) {
		double facteurEclair = p.getFacteurEclairage(Figure.SOURCE_ECLAIRAGE);
		Color RGB = fillColor.getValue();
		if(facteurEclair > 0) {
			gc.setFill(Color.rgb((int) Math.round(facteurEclair * RGB.getRed() * 255), (int) Math.round(facteurEclair * RGB.getGreen() * 255), (int) Math.round(facteurEclair * RGB.getBlue() * 255)));
		} else {
			gc.setFill(Color.BLACK);
		}
	}


	/**
	 * nettoie la totalite du canvas
	 */
	public void clearCanvas() {
		gc.clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
	}
	
	@Override
	public void update(Observable o, Object arg) {
		reloadCanvas();
	}
}
