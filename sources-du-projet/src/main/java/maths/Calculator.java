package maths;

import java.util.Arrays;
import java.util.List;

import model.Figure;
import model.Polygon;
import model.Vertex;
import util.BenchmarkHelper;


/**
 * Classe ne contenant que des methodes statiques (aucun interet de l'instancier) et qui permet d'effectuer des transformations sur une figure ou un vecteur
 * @author clotaire.dufresne.etu
 * 
 */
public class Calculator {

	/**
	 * Calcule un ensemble de transformations sur une figure
	 * @param f Figure a modifier
	 * @param transformations ensemble de transformations qui seront appliquees a f
	 */
	public static void calculate(Figure f, Transformation...transformations) {
		BenchmarkHelper benchmarkHelper = new BenchmarkHelper();

		for(Transformation t : transformations) {
/*
			!!!!! Problème de netoyage des threads (jamais arrêté)
			TransformerFactory factory = new TransformerFactory(f, t);
			ArrayList<VerticesTransformer> threads = factory.makeThreads();
			ExecutorService exec = Executors.newCachedThreadPool();
			try {
				System.out.println("NB DE THREADS :" + threads.size());
				exec.invokeAll(threads);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
*/

			for(Vertex v : f.getVertices()) {
				t.calculate(v);
			}
			for(Polygon p : f.getPolygons()) {
				t.calculateVNU(p.getVectNormUnit());
			}
		}

		System.out.println("Transformations " + Arrays.toString(transformations) + " made in " + benchmarkHelper.getMilliSeconds() + " milliseconds");
		f.changed();
	}
	
	/**
	 * applique des transformations
	 * @param listeSommets l'ensemble des sommets à transformer
	 * @param transformations les transformations à appliquer
	 */
	public static void calculate(List<Vertex> listeSommets, Transformation...transformations) {
		for(Vertex v : listeSommets) {
			Calculator.calculate(v, transformations);
		}
	}
	
	/**
	 * Calcule un ensemble de transformations sur un sommet
	 * @param sommet sommet a modifier
	 * @param transformations ensemble de transformations qui seront appliquées a v
	 */
	public static void calculate(Vertex sommet, Transformation...transformations) {
		for(Transformation t : transformations) {
			t.calculate(sommet);
		}
	}
	
	/**
	 * 
	 * @param v sommet a copier
	 * @param transformations transformations ensemble de transformations qui seront appliquées a la copie
	 * @return une copie du sommet v s'il avait subit les transformations donnees
	 */
	public static Vertex calculateCopy(Vertex v, Transformation...transformations) {
		Vertex res = new Vertex(v.getX(), v.getY(), v.getZ());
		Calculator.calculate(res, transformations);
		return res;
	}
}
