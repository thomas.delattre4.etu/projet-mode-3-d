package mathsTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

import maths.*;
import org.junit.jupiter.api.Test;

import maths.Calculator;
import maths.Homothetie;
import maths.RotationX;
import maths.RotationY;
import maths.RotationZ;
import maths.TransRotaX;
import maths.TransRotaY;
import maths.TransRotaZ;
import maths.Translation;
import model.Vertex;

import java.util.List;

public class CalculsTest {
	
	@Test
	void homothetieTest() {
		Vertex value = new Vertex(1,2,3);
		Vertex expected = new Vertex(2,4,6);
		assertEquals(expected, Calculator.calculateCopy(value, new Homothetie(2.0)));
		
		expected = new Vertex(-2, -4, -6);
		Calculator.calculate(value, new Homothetie(-2.0));
		assertEquals(expected, value);
	}
	
	@Test
	void translationTest() {
		Vertex expected = new Vertex(1,2,3);
		Vertex value = new Vertex(0,0,0);
		Calculator.calculate(value, new Translation(new Vertex(1,2,3)));
		assertEquals(expected, value);
	}
	
	@Test
	void rotationXTest() {
		Vertex value = new Vertex(1,1,1);
		Vertex expected = new Vertex(1,1,1);
		expected.setY(1*Math.cos(Math.PI/2)-1*Math.sin(Math.PI/2));
		expected.setZ(1*Math.sin(Math.PI/2)+1*Math.cos(Math.PI/2));
		Calculator.calculate(value, new RotationX(Math.PI/2));
		
		assertEquals(expected, value);
	}
	
	@Test
	void rotationYTest() {
		Vertex value = new Vertex(1,1,1);
		Vertex expected = new Vertex(1,1,1);
		expected.setX(1*Math.cos(Math.PI/4) - 1*Math.sin(Math.PI/4));
		expected.setZ(1*Math.sin(Math.PI/4) + 1*Math.cos(Math.PI/4));
		Calculator.calculate(value, new RotationY(Math.PI/4));
		
		assertEquals(expected, value);
	}
	
	@Test
	void rotationZTest() {
		Vertex value = new Vertex(1,1,1);
		Vertex expected = new Vertex(1,1,1);
		expected.setX(1*Math.cos(Math.PI/4) - 1*Math.sin(Math.PI/4));
		expected.setY(1*Math.sin(Math.PI/4) + 1*Math.cos(Math.PI/4));
		Calculator.calculate(value, new RotationZ(Math.PI/4));
		
		assertEquals(expected, value);
	}
	
	@Test
	void multiplesTransformationsTest() {
		Vertex expected = new Vertex(2,2,2); // translation deja appliquee
		Vertex value = new Vertex(1,1,1);
		expected.setY(2*Math.cos(Math.PI/2)-2*Math.sin(Math.PI/2));
		expected.setZ(2*Math.sin(Math.PI/2)+2*Math.cos(Math.PI/2));
		
		Calculator.calculate(value, new Translation(new Vertex(1,1,1)), new RotationX(Math.PI/2));
		
		assertEquals(expected, value);
		
		Homothetie h = new Homothetie(5.0);
		RotationY ry = new RotationY(5.0);
		Calculator.calculate(expected, h, ry);
		Calculator.calculate(value, h);
		Calculator.calculate(value, ry);
		
		assertEquals(expected, value);
	}

	@Test
	void transRotaTest() {
		Vertex expected = new Vertex(1,1,1);
		Vertex actual = new Vertex(1,1,1);

		double angle = 10.0;
		Vertex translation = new Vertex(2,2,2);
		Vertex reversedTrans = Calculator.calculateCopy(translation, new Homothetie(Vertex.INVERSE));
		Translation reverser = new Translation(reversedTrans);
		Translation normalizer = new Translation(translation);

		Calculator.calculate(expected, reverser,new RotationX(angle), normalizer);
		Calculator.calculate(actual, new TransRotaX(translation, angle));

		assertEquals(expected, actual);

		expected = new Vertex(1,1,1);
		actual = new Vertex(1,1,1);

		Calculator.calculate(expected, reverser,new RotationY(angle), normalizer);
		Calculator.calculate(actual, new TransRotaY(translation, angle));

		assertEquals(expected, actual);


		expected = new Vertex(1,1,1);
		actual = new Vertex(1,1,1);

		Calculator.calculate(expected, reverser,new RotationZ(angle), normalizer);
		Calculator.calculate(actual, new TransRotaZ(translation, angle));

		assertEquals(expected, actual);
	}


	@Test
	void benchmarkCalcul() {
		Vertex expected = new Vertex(2,2,2); // translation deja appliquee
		Vertex value = new Vertex(1,1,1);

		int NUMBER_OF_TRANSFORMATIONS = 100_000_000;

		Transformation[] transformations = new Transformation[]{
				new Translation(new Vertex(1,1,1)),
				new RotationX(Math.PI/2),
				new RotationZ(Math.PI/4),
				new RotationY(Math.PI/4),
				new Homothetie(1.0)
		};

		Transformation[] transformations1 =  new Transformation[NUMBER_OF_TRANSFORMATIONS];

		for(int i = 0; i < NUMBER_OF_TRANSFORMATIONS; i++) {
			transformations1[i] = transformations[i%5];
		}

		System.out.println(value);
		Calculator.calculate(value, transformations1);
		System.out.println(value);
	}
}
