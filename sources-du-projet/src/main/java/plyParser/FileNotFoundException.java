package plyParser;

import java.io.File;

public class FileNotFoundException extends AlertBoxAbleException {

    private static final long serialVersionUID = 1L;

    public FileNotFoundException(String errorMessage) {
        super(errorMessage);
    }

    public String errorTitle() {
        return "Erreur fichier introuvable";
    }

    public String errorHeaderText(File file) {
        return "Le fichier " + file.getName() + " est introuvable";
    }
}
