package maths;

import model.Vertex;

public abstract class Transformation {

	/**
	 * Calcule une certaine transformation sur sommet v
	 * @param v
	 */
	public abstract void calculate(Vertex v);
	
	/**
	 * Recalcule le vecteur normal unitaire si necessaire
	 * @param vnu le vecteur normal unitaire
	 */
	public abstract void calculateVNU(Vertex vnu);
	
	/**
	 * Calcule une copie du sommet et lui applique un calcul
	 * @param v sommet a copier
	 * @return une copie du sommet
	 */
	public Vertex copyOfCalculate(Vertex v) {
		Vertex res = new Vertex(v.getX(), v.getY(), v.getZ());
		this.calculate(res);
		return res;
	}
	
	/**
	 * Affecte des nouvelles valeurs au sommet passé en parametre
	 * @param v sommet a modifier
	 * @param newX nouvelle valeur x
	 * @param newY nouvelle valeur x
	 * @param newZ nouvelle valeur x
	 */
	protected void setVertexNewValues(Vertex v, double newX, double newY, double newZ) {
		v.setX(newX);
		v.setY(newY);
		v.setZ(newZ);
	}
	
	
}
