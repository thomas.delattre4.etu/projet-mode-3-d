package mathsTest;


import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import maths.Calculator;
import maths.Homothetie;
import maths.RotationX;
import maths.RotationY;
import maths.RotationZ;
import maths.Translation;
import model.Vertex;

public class CalculsTest {
	
	@Test
	void homothetieTest() {
		Vertex value = new Vertex(1,2,3);
		Vertex expected = new Vertex(2,4,6);
		assertEquals(expected, Calculator.calculateCopy(value, new Homothetie(2.0)));
		
		expected = new Vertex(-2, -4, -6);
		Calculator.calculate(value, new Homothetie(-2.0));
		assertEquals(expected, value);
	}
	
	@Test
	void translationTest() {
		Vertex expected = new Vertex(1,2,3);
		Vertex value = new Vertex(0,0,0);
		Calculator.calculate(value, new Translation(new Vertex(1,2,3)));
		assertEquals(expected, value);
	}
	
	@Test
	void rotationXTest() {
		Vertex value = new Vertex(1,1,1);
		Vertex expected = new Vertex(1,1,1);
		expected.setY(1*Math.cos(Math.PI/2)-1*Math.sin(Math.PI/2));
		expected.setZ(1*Math.sin(Math.PI/2)+1*Math.cos(Math.PI/2));
		Calculator.calculate(value, new RotationX(Math.PI/2));
		
		assertEquals(expected, value);
	}
	
	@Test
	void rotationYTest() {
		Vertex value = new Vertex(1,1,1);
		Vertex expected = new Vertex(1,1,1);
		expected.setX(1*Math.cos(Math.PI/4) - 1*Math.sin(Math.PI/4));
		expected.setZ(1*Math.sin(Math.PI/4) + 1*Math.cos(Math.PI/4));
		Calculator.calculate(value, new RotationY(Math.PI/4));
		
		assertEquals(expected, value);
	}
	
	@Test
	void rotationZTest() {
		Vertex value = new Vertex(1,1,1);
		Vertex expected = new Vertex(1,1,1);
		expected.setX(1*Math.cos(Math.PI/4) - 1*Math.sin(Math.PI/4));
		expected.setY(1*Math.sin(Math.PI/4) + 1*Math.cos(Math.PI/4));
		Calculator.calculate(value, new RotationZ(Math.PI/4));
		
		assertEquals(expected, value);
	}
	
	@Test
	void multiplesTransformationsTest() {
		Vertex expected = new Vertex(2,2,2); // translation deja appliquee
		Vertex value = new Vertex(1,1,1);
		expected.setY(2*Math.cos(Math.PI/2)-2*Math.sin(Math.PI/2));
		expected.setZ(2*Math.sin(Math.PI/2)+2*Math.cos(Math.PI/2));
		
		Calculator.calculate(value, new Translation(new Vertex(1,1,1)), new RotationX(Math.PI/2));
		
		assertEquals(expected, value);
		
		Homothetie h = new Homothetie(5.0);
		RotationY ry = new RotationY(5.0);
		Calculator.calculate(expected, h, ry);
		Calculator.calculate(value, h);
		Calculator.calculate(value, ry);
		
		assertEquals(expected, value);
	}
	
}
