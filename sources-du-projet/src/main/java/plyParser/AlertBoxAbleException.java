package plyParser;

import java.io.File;

public abstract class AlertBoxAbleException extends RuntimeException {
    public AlertBoxAbleException(String message) {
        super(message);
    }

    public abstract String errorTitle();
    public abstract String errorHeaderText(File file);
}
