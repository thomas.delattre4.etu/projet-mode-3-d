package view;
import java.io.IOException;

import controller.LibrariesInterfaceController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

/**
 * Classe qui demarre l'interface en chargeant le fichier fxml
 * @author adil.benameur
 *
 */
public class Interface extends Application {
    public void start(Stage stage) throws IOException {
        FXMLLoader loader = new FXMLLoader(Interface.class.getResource("Libraries.fxml"));
        BorderPane borderPane = loader.load();
        ((LibrariesInterfaceController)loader.getController()).setStage(stage);

        Scene scene = new Scene(borderPane);

        stage.setMinWidth(900);
        stage.setMinHeight(430);

        stage.setScene(scene);
        stage.setTitle("3D Ply");
        stage.show();
    }

    public static void main(String[] args) {
            Application.launch(args);
    }
}
