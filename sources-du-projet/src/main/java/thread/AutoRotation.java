package thread;

import javafx.animation.AnimationTimer;

public class AutoRotation extends AnimationTimer {

	private RotationFunction rotationFunction;
	private double pas;

	public interface RotationFunction {
		void rotate(double pas);
	}

	public AutoRotation(RotationFunction rotationFunction, double pas) {
		this.rotationFunction = rotationFunction;
		this.pas = pas;
	}

	@Override
	public void handle(long now) {
		rotationFunction.rotate(pas);
	}
}