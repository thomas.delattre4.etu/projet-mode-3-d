package plyParser;

import java.util.regex.Pattern;

/**
 * Classe contenant les fonctions générales nécessaires pour la validation de chaine
 */
class ValidationHelper {
    private final PlyReader plyReader;

    public ValidationHelper(PlyReader plyReader) {
        this.plyReader = plyReader;
    }

    /**
     * Valide la chaine passer en paramètre. Lève une exception si la chaine n'est pas valide.
     * @param pattern regex
     * @param line line à valider
     * @return retourne la ligne validée
     */
    public String validate(Pattern pattern, String line) {
        if (pattern.matcher(line).find()) {
            return line;
        }
        else throw WrongFileFormatException.errorAtLine(line, plyReader.lineNumber());
    }
}
