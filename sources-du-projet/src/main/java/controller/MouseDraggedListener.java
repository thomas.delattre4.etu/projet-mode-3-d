package controller;

import javafx.event.EventHandler;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import model.Vertex;

/**
 * Classe qui gere les mouvements a la souris tels que la rotation (clic principal) et le deplacement (clic secondaire)
 * @author clotaire.dufresne.etu
 *
 */
public class MouseDraggedListener implements EventHandler<MouseEvent> {
	
	private Vertex dragMousePosition;
	private FigureController figureController;
	
	/**
	 * stocke le figureController necessaire
	 * @param figureController figure controleur qui sera utilise
	 */
	public MouseDraggedListener(FigureController figureController) {
		this.figureController = figureController;
	}
	
	@Override
	public void handle(MouseEvent event) {
		if(dragMousePosition != null) {
			double xDifference = (event.getX() - dragMousePosition.getX());
			double yDifference = (event.getY() - dragMousePosition.getY());

			// rotation
			if(event.getButton() == MouseButton.PRIMARY) {
				figureController.rotate(xDifference, yDifference, 0);
			}
			// translation
			else if(event.getButton() == MouseButton.SECONDARY) {
				figureController.translate(xDifference, -yDifference);
			}
		}

		if(dragMousePosition == null) dragMousePosition = new Vertex(0,0,0);

		dragMousePosition.setX(event.getX());
		dragMousePosition.setY(event.getY());
	}
	
	/**
	 * redefinit le sommet dragMousePosition
	 * @param newDragMousePosition nouvelle valeur
	 */
	public void setDragMousePosition(Vertex newDragMousePosition) {
		this.dragMousePosition = newDragMousePosition;
	}
	
}
