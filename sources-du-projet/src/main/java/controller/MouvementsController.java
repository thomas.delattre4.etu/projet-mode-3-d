package controller;

import javafx.beans.property.Property;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import thread.AutoRotation;

public class MouvementsController {
    private FigureController figureController;

    private final double ROTATION_AMPLIFIER = 20;

    public Button translationHaut;
    public Button translationGauche;
    public Button translationDroite;
    public Button translationBas;
    public Slider homotethie;
    public Slider rotationX;
    public Slider rotationY;
    public Slider rotationZ;

    public ToggleButton autoXPlus;
    public ToggleButton autoXMinus;
    public ToggleButton autoYPlus;
    public ToggleButton autoYMinus;
    public ToggleButton autoZPlus;
    public ToggleButton autoZMinus;

    public ColorPicker segmentsColorPicker;
    public ColorPicker facesColorPicker;


    /**
     * Paramètre l'interface des mouvements
     * @param figureController
     */
    public void initialize(FigureController figureController) {
        this.figureController = figureController;
        ToggleGroup xToggleGroup = new ToggleGroup();
        ToggleGroup yToggleGroup = new ToggleGroup();
        ToggleGroup zToggleGroup = new ToggleGroup();

        autoXPlus.setToggleGroup(xToggleGroup);
        autoXMinus.setToggleGroup(xToggleGroup);

        autoYPlus.setToggleGroup(yToggleGroup);
        autoYMinus.setToggleGroup(yToggleGroup);

        autoZPlus.setToggleGroup(zToggleGroup);
        autoZMinus.setToggleGroup(zToggleGroup);

        AutoRotation xAutoRotationPlus = new AutoRotation(figureController::rotateX, ROTATION_AMPLIFIER);
        AutoRotation xAutoRotationMinus = new AutoRotation(figureController::rotateX, -ROTATION_AMPLIFIER);

        AutoRotation yAutoRotationPlus = new AutoRotation(figureController::rotateY, ROTATION_AMPLIFIER);
        AutoRotation yAutoRotationMinus = new AutoRotation(figureController::rotateY, -ROTATION_AMPLIFIER);

        AutoRotation zAutoRotationPlus = new AutoRotation(figureController::rotateZ, ROTATION_AMPLIFIER);
        AutoRotation zAutoRotationMinus = new AutoRotation(figureController::rotateZ, -ROTATION_AMPLIFIER);

        setAutoRotation(xToggleGroup, xAutoRotationPlus, xAutoRotationMinus, autoXPlus, autoXMinus);
        setAutoRotation(yToggleGroup, yAutoRotationPlus, yAutoRotationMinus, autoYPlus, autoYMinus);
        setAutoRotation(zToggleGroup, zAutoRotationPlus, zAutoRotationMinus, autoZPlus, autoZMinus);


        // Slider cursor position
        homotethie.setValue((homotethie.getMax() - homotethie.getMin()) / 2);

        rotationY.setValue((rotationY.getMax() - rotationY.getMin()) / 2);
        rotationX.setValue((rotationX.getMax() - rotationX.getMin()) / 2);
        rotationZ.setValue((rotationZ.getMax() - rotationZ.getMin()) / 2);


        // Listener
        translationHaut.setOnAction(translationChangeListener(0, 10));
        translationGauche.setOnAction(translationChangeListener(-10, 0));
        translationDroite.setOnAction(translationChangeListener(10, 0));
        translationBas.setOnAction(translationChangeListener(0,-10));

        homotethie.valueProperty().addListener((observable, oldValue, newValue) -> {
            figureController.zoom(
                    newValue.doubleValue() - oldValue.doubleValue() > 0 ? FigureController.ZOOM.IN : FigureController.ZOOM.OUT
            );
        });


        rotationX.valueProperty().addListener((observable, oldValue, newValue) -> {
            figureController.rotateX((newValue.doubleValue() - oldValue.doubleValue())*ROTATION_AMPLIFIER);
        });

        rotationY.valueProperty().addListener((observable, oldValue, newValue) -> {
            figureController.rotateY((newValue.doubleValue() - oldValue.doubleValue())*ROTATION_AMPLIFIER);
        });

        rotationZ.valueProperty().addListener((observable, oldValue, newValue) -> {
            figureController.rotateZ((newValue.doubleValue() - oldValue.doubleValue())*ROTATION_AMPLIFIER);
        });
    }

    private void setAutoRotation(ToggleGroup toggleGroup, AutoRotation autoRotationPlus, AutoRotation autoRotationMinus, ToggleButton autoPlus, ToggleButton autoMinus) {
        toggleGroup.selectedToggleProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue == null) {
                autoRotationPlus.stop();
                autoRotationMinus.stop();
            } else if (newValue == autoPlus) {
                autoRotationMinus.stop();
                autoRotationPlus.start();
            } else if (newValue == autoMinus) {
                autoRotationPlus.stop();
                autoRotationMinus.start();
            }
        });
    }


    public void setColorPikers(Property<Color> fillColor, Property<Color> strokeColor) {
        segmentsColorPicker.setValue(strokeColor.getValue());
        facesColorPicker.setValue(fillColor.getValue());

        facesColorPicker.setOnAction(event -> {
            fillColor.setValue(facesColorPicker.getValue());
        });

        segmentsColorPicker.setOnAction(event -> {
            strokeColor.setValue(segmentsColorPicker.getValue());
        });
    }

    /**
     * Génére un change listener en fonction d'un mouvement (x,y)
     * @param x
     * @param y
     * @return
     */
    private EventHandler<ActionEvent> translationChangeListener(double x, double y) {
        return (event) -> {
            figureController.translate(x, y);
        };
    }
}
