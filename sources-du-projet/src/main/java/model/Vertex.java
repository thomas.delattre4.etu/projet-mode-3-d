package model;

import java.util.HashMap;
import java.util.Objects;

/**
 * Sommets represente par 3 coordonnes X, Y et Z
 * @author adil.benameur.etu
 *
 */
public class Vertex implements Cloneable {
	private double x,y,z;
	private HashMap<String, Double> otherProperties;
	public static final double INVERSE = -1.0;

	/**
	 * cree un nouveau sommet de valeur x, y et z
	 * @param x coordonnee x
	 * @param y coordonnee y
	 * @param z coordonnee z
	 */
	public Vertex(double x, double y, double z) {
		this(x,y,z,new HashMap<>());
	}

	/**
	 * cree un nouveau sommet de valeur x, y et z et ayant d'autres proprietes
	 * @param x coordonnee x
	 * @param y coordonnee y
	 * @param z coordonnee z
	 * @param otherProperties proprietes diverses
	 */
	public Vertex(double x, double y, double z, HashMap<String, Double> otherProperties) {
		this.otherProperties = otherProperties;
		this.x = x;
		this.y = y;
		this.z = z;
	}

	/**
	 * recupere la coordonnee x
	 * @return coordonnee x
	 */
	public double getX() {
		return x;
	}

	/**
	 * recupere la coordonnee y
	 * @return coordonnee y
	 */
	public double getY() {
		return y;
	}

	/**
	 * recupere la coordonnee z
	 * @return coordonnee z
	 */
	public double getZ() {
		return z;
	}
	
	/**
	 * recupere la propriete liee au nom de cette propriete
	 * @param property nom de la propriete
	 * @return valeur de la propriete
	 */
	public double get(String property) {
		Double value = otherProperties.get(property);
		if(value == null) throw new PropertyDoesntExistException("Property " + property + " doesn't exist in vertex.");
		return value;
	}

	/**
	 * redefinit la valeur x
	 * @param x nouvelle valeur de x
	 */
	public void setX(double x) {
		this.x = x;
	}

	/**
	 * redefinit la valeur y
	 * @param y nouvelle valeur de y
	 */
	public void setY(double y) {
		this.y = y;
	}

	/**
	 * redefinit la valeur z
	 * @param z nouvelle valeur de z
	 */
	public void setZ(double z) {
		this.z = z;
	}

	/**
	 * redefinit la valeur de la propriete demandee
	 * @param property la propriete a modifier
	 * @param value la nouvelle valeur
	 */
	public void set(String property, double value) {
		otherProperties.put(property, value);
	}
	
	/**
	 * fait le calcul ce sommet moins un autre vecteur
	 * @param toSubstract sommet a soustraire
	 * @return un sommet resultant de l'operation
	 */
	public Vertex moinsVertex(Vertex toSubstract) {
		return new Vertex(this.x - toSubstract.getX(), this.y - toSubstract.getY(), this.z - toSubstract.getZ());
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Vertex vertex = (Vertex) o;
		return Double.compare(vertex.x, x) == 0 &&
				Double.compare(vertex.y, y) == 0 &&
				Double.compare(vertex.z, z) == 0 ;
				//Objects.equals(otherProperties, vertex.otherProperties);
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y, z, otherProperties);
	}

	/**
	 * exception renvoyee lorsque la propriete demandee n'existe pas
	 * @author adil.benameur.etu
	 *
	 */
	private class PropertyDoesntExistException extends RuntimeException {
		private static final long serialVersionUID = 1L;

		public PropertyDoesntExistException(String message) {
			super(message);
		}
	}

	@Override
	public String toString() {
		return "Vertex{" +
				"x=" + x +
				", y=" + y +
				", z=" + z +
				'}';
	}
	
	public double getNorme() {
		return Math.sqrt(Math.pow(x, 2) + Math.pow(y, 2) + Math.pow(z, 2));
	}

	@Override
	protected Vertex clone() throws CloneNotSupportedException {
		return new Vertex(x, y, z, (HashMap<String, Double>) otherProperties.clone());
	}
	
}
