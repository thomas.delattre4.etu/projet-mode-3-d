package model;

import java.util.ArrayList;
import java.util.List;

import maths.Calculator;
import maths.Homothetie;
import maths.Translation;

public class OmbreFigure extends Figure {
	
	private final double PROFONDEUR_OMBRE = -10;
	private final Vertex DIRECTION_LUM;
	
	public OmbreFigure(Figure aOmbrer) {
		this.DIRECTION_LUM = Calculator.calculateCopy(Figure.SOURCE_ECLAIRAGE, new Homothetie(-1.0));
		super.polygons = (ArrayList<Polygon>) ombrage(aOmbrer);
		super.setVertices((ArrayList<Vertex>) recuperer(super.polygons));
		double depth = -aOmbrer.getFigureZLength() + PROFONDEUR_OMBRE + DIRECTION_LUM.getZ();
		Vertex deplacement = new Vertex(DIRECTION_LUM.getX()-50, DIRECTION_LUM.getY()-50, depth);
		Calculator.calculate(super.vertices, new Translation(deplacement));
	}

	/**
	 * calcule la liste des polygones de l'ombre. puisqu'on affiche sur un axe u(1,0,0) et v(0,1,0) il suffit de modifier la profondeur z de chacun des points puis d'additionner le vecteur lumière (dans le sens source vers origine
	 * @return la liste des polygones de l'ombre
	 */
	private List<Polygon> ombrage(Figure aOmbrer) {
		ArrayList<Polygon> ombre = new ArrayList<>();//(ArrayList<Polygon>) aOmbrer.getPolygons().clone();
		for(Polygon p : aOmbrer.getPolygons()) {
			Vertex[] vertices = p.getVertices();
			try {
				ombre.add(new Polygon(vertices[0].clone(), vertices[1].clone(), vertices[2].clone()));
			} catch (CloneNotSupportedException e) {
				e.printStackTrace();
			}
		}
		return ombre;
	}

	private List<Vertex> recuperer(ArrayList<Polygon> ombre) {
		ArrayList<Vertex> vertices = new ArrayList<>();
		for(Polygon p : ombre) {
			Vertex[] polygonVertices = p.getVertices();
			for(Vertex v : polygonVertices) {
				if(!vertices.contains(v)) {
					vertices.add(v);
				}
			}
		}
		
		return vertices;
	}
	
	public ArrayList<Polygon> getOmbre() {
		return super.polygons;
	}
	
	public ArrayList<Vertex> getPtsOmbre() {
		return super.vertices;
	}
	
}

/*
 * R I P
 * 2020 - 2020 
 */
