package model;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import maths.Calculator;
import maths.Homothetie;
import maths.SimpleCalculations;

/**
 * Polygone, represente par un ensemble x de sommets
 * @author adil.benameur.etu
 *
 */
public class Polygon implements Comparable<Polygon>{
	private final Vertex[] vertices;
	private Vertex vectNormUnit;


	/**
	 * definit l'ensemble de sommets
	 * @param vertices ensemble de sommets
	 */
	public Polygon(Vertex... vertices) {
		this.vertices = vertices;
		if(vertices.length == 3) {
			this.calculVectNormUnit();
		}
	}

	/**
	 * definit l'ensemble de sommets en transformant la liste en tableau
	 * @param vertices ensemble de sommets
	 */
	public Polygon(List<Vertex> vertices) {
		this.vertices = new Vertex[vertices.size()];

		for (int i = 0; i < this.vertices.length; i++) {
			this.vertices[i] = vertices.get(i);
		}
		
		if(vertices.size() == 3) {
			this.calculVectNormUnit();
		}
	}
	
	/**
	 * recupere l'ensemble de sommets
	 * @return un tableau des sommets
	 */
	public Vertex[] getVertices() {
		return this.vertices;
	}

	/**
	 * recupere le nombre de sommets
	 * @return le nombre de sommets
	 */
	public int getVerticesNumber() {
		return vertices.length;
	}
	
	/**
	 * renvoie le vecteur normal unitaire li� � cette face
	 * @return Vertex - le vecteur normal unitaire
	 */
	public Vertex getVectNormUnit() {
		return vectNormUnit;
	}
	
	/**
	 * recupere l'ensemble des valeurs X, Y ou Z selon ce qui est demande
	 * @param wanted valeur de ce qui est attendue. 0 pour les valeurs de x, 1 pour y, 2 pour z
	 * @return tableau des valeurs X, Y ou Z
	 */
	private double[] getXYZ(int wanted) {
		double[] values = new double[vertices.length];

		for (int i = 0; i < vertices.length; i++) {
			if(wanted == 0) values[i] = vertices[i].getX();
			else if(wanted == 1) values[i] = vertices[i].getY();
			else values[i] = vertices[i].getZ();
		}

		return values;
	}
 
	/**
	 * recupere l'ensemble des valeurs de X de chaque sommet
	 * @return tableau des valeurs de X
	 */
	public double[] getX() {
		return this.getXYZ(0);
	}

	/**
	 * recupere l'ensemble des valeurs de Y de chaque sommet
	 * @return tableau des valeurs de Y
	 */
	public double[] getY() {
		return this.getXYZ(1);
	}
	
	/**
	 * recupere l'ensemble des valeurs de Z de chaque sommet
	 * @return tableau des valeurs de Z
	 */
	public double[] getZ() {
		return this.getXYZ(2);
	}

	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		Polygon polygon = (Polygon) o;
		return Arrays.equals(vertices, polygon.vertices);
	}

	@Override
	public int hashCode() {
		return Arrays.hashCode(vertices);
	}

	@Override
	public String toString() {
		return "Polygon{" +
				"vertices=" + Arrays.toString(vertices) +
				'}';
	}

	@Override
	public int compareTo(Polygon o) {
		double[] thisZValues = this.getZ();
		double[] oZValues = o.getZ();
		double thisZSum = SimpleCalculations.sum(thisZValues);
		double oZSum = SimpleCalculations.sum(oZValues);
		
		if(thisZSum > oZSum) return 1;
		else if(thisZSum < oZSum) return -1;
		
		return 0;
	}
	
	private void calculVectNormUnit() {
		Vertex ab = this.vertices[1].moinsVertex(this.vertices[0]);
		Vertex ac = this.vertices[2].moinsVertex(this.vertices[0]);
		Vertex vnu = SimpleCalculations.produitVectoriel(ab, ac);
		
		SimpleCalculations.rendreUnitaire(vnu);
		
		this.vectNormUnit = vnu;
	}
	
	public double getFacteurEclairage(Vertex source) {
		double res = 0;
		Vertex srcCpy = new Vertex(source.getX(), source.getY(), source.getZ());
		SimpleCalculations.rendreUnitaire(srcCpy);
	
		res = srcCpy.getX()*vectNormUnit.getX() + srcCpy.getY()*vectNormUnit.getY() + srcCpy.getZ()*vectNormUnit.getZ();
		
		return res;
	}
	
	@Override
	protected Polygon clone() throws CloneNotSupportedException {
		return new Polygon(this.vertices.clone());
	}
}
