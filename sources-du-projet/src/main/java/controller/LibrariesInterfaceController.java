package controller;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.CheckMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.BorderPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import model.Figure;
import model.FigureInfo;
import plyParser.Parser;
import plyParser.WrongFileFormatException;
import view.Interface;
import view.components.FileListCell;

public class LibrariesInterfaceController implements Initializable {
    public TableView<FigureInfo> plyTableView;
    public MenuItem openLibraryMenuItem;
    public MenuItem removeLibraryMenuItem;
    public MenuItem closeMenuItem;
    public ListView<File> libraryListView;
    public Label descriptionLabel;
    public Canvas previewCanvas;
    public CheckMenuItem rotationAuto;

    private List<Stage> subStages = new ArrayList<>();

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        PreviewController previewController = new PreviewController(previewCanvas, rotationAuto);

        plyTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
        plyTableView.setPlaceholder(new Label("Aucune librairie chargée ou librairie vide"));

        ObservableList<FigureInfo> infos = FXCollections.observableArrayList();
        plyTableView.setItems(infos);

        plyTableView.getColumns().get(0).setCellValueFactory(new PropertyValueFactory<>("fileName"));
        plyTableView.getColumns().get(1).setCellValueFactory(new PropertyValueFactory<>("figureName"));
        plyTableView.getColumns().get(2).setCellValueFactory(new PropertyValueFactory<>("creationDate"));
        plyTableView.getColumns().get(3).setCellValueFactory(new PropertyValueFactory<>("author"));

        libraryListView.setCellFactory(param -> new FileListCell(libraryListView));

        libraryListView.getSelectionModel().getSelectedItems().addListener((ListChangeListener<File>) c -> {
            infos.clear();

            if (c.next() && c.wasAdded()) {
                File f = c.getAddedSubList().get(0);

                for (File plyFile: f.listFiles()) {
                    try {
                        FigureInfo info = new Parser(plyFile).getInformations();
                        infos.add(info);
                    } catch (WrongFileFormatException e) {
                        e.printStackTrace();
                    }
                }
            }
        });

        plyTableView.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) -> {
            if(newValue != null) {
                if(newValue.getDescription() == null)
                    descriptionLabel.setText("Description indisponible");
                else
                    descriptionLabel.setText(newValue.getDescription());

                previewController.setInfo(newValue);
            }
        });

        plyTableView.setOnMouseClicked(event -> {
            if(event.getClickCount() > 1) {
                try {
                    FigureInfo figureInfo = plyTableView.getSelectionModel().getSelectedItem();

                    Stage stage = new Stage();
                    subStages.add(stage);
                    FXMLLoader loader = new FXMLLoader(Interface.class.getResource("Interface3D.fxml"));
                    BorderPane rightAnchorPane = loader.load();
                    Figure f = new Parser(figureInfo.getFile()).getFigure();
                    ((Interface3DController)loader.getController()).setFigureAndStage(f,stage);
                    Scene scene = new Scene(rightAnchorPane);
                    stage.setMinWidth(845);
                    stage.setMinHeight(515);

                    stage.setScene(scene);
                    stage.setTitle("3D Ply - " + figureInfo.getFileName());
                    stage.show();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

        removeLibraryMenuItem.setOnAction(event -> {
            libraryListView.getItems().remove(
                    libraryListView.getSelectionModel().selectedItemProperty().get()
            );
        });


        //////// OPEN A DEFAULT LIB AT STARTUP
        File defaultLib = new File("ressources/library");
        libraryListView.getItems().add(defaultLib);
        libraryListView.getSelectionModel().select(defaultLib);
    }

    public void setStage(Stage stage) {
        stage.setOnCloseRequest(event -> {
            if(event.getEventType() == WindowEvent.WINDOW_CLOSE_REQUEST)
                for (Stage subStage : subStages)
                    subStage.close();
        });

        openLibraryMenuItem.setOnAction(event -> {
            DirectoryChooser directoryChooser = new DirectoryChooser();
            File selectedDirectory = directoryChooser.showDialog(stage);

            if(selectedDirectory != null && !libraryListView.getItems().contains(selectedDirectory)) {
                libraryListView.getItems().add(selectedDirectory);
                libraryListView.getSelectionModel().select(selectedDirectory);
            }
        });
    }
}
