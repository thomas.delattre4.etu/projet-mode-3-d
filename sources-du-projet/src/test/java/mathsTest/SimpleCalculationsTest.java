package mathsTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import maths.SimpleCalculations;
import model.Vertex;

public class SimpleCalculationsTest {

	
	@Test
	public void sumTest() {
		double[] values = {5.0, 2.0, 30.0, 5.0};
		double expected = 42.0;
		
		assertEquals(expected, SimpleCalculations.sum(values));
		assertEquals(0, SimpleCalculations.sum(new double[10]));
	}
	
	@Test
	public void avgTest() {
		Double[] values = {5.0, 2.0, 30.0, 5.0};
		double expected = 10.5;
		
		assertEquals(expected, SimpleCalculations.average(values));
		assertEquals(0, SimpleCalculations.average(new Double[10]));
	}
	
	@Test
	void produitVectorielTest() {
		Vertex vectX = new Vertex(1, 1, 0);
		Vertex vectY = new Vertex(1, 0, 2);
		Vertex expected = new Vertex(2, -2, -1);
		
		assertEquals(expected, SimpleCalculations.produitVectoriel(vectX, vectY));
	}
}
