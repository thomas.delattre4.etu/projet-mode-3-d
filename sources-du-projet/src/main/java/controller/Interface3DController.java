package controller;

import javafx.beans.property.Property;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import model.Figure;
import plyParser.PlyWriter;
import view.Interface;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

/**
 * Classe permettant le controle des differentes parties : canvas, liste fichiers ply disponibles, menus...
 * @author adil.benameur.etu
 */
public class Interface3DController implements Initializable {
	public Canvas canvas;
	public MenuItem saveAsMenuItem;
	public MenuItem saveMenuItem;
	public MenuItem centerMenuItem;
	public CheckMenuItem displayFacesMenuItem;
	public CheckMenuItem displaySegmentsMenuItem;
	public CheckMenuItem displayShadowMenuItem;
	public BorderPane borderPane;
	public TableView<Property> propertiesTableView;
	public TableColumn<Property, String> propertyNameTableColumn;
	public TableColumn<Property, String> propertyValueTableColumn;

	public static class Property {
		String propertyName;
		String propertyValue;

		public Property(String propertyName, String propertyValue) {
			this.propertyName = propertyName;
			this.propertyValue = propertyValue;
		}

		public String getPropertyName() {
			return propertyName;
		}

		public String getPropertyValue() {
			return propertyValue;
		}
	}

	public void initialize(URL location, ResourceBundle resources) {
		canvasResizeAble();
	}

	/**
	 * Ajoute l'interface de contrôle mouvements à la borderpane à droite
	 */
	private void addMouvementsControlRightBorderPane(FigureController figureController, javafx.beans.property.Property<Color> fillColor, javafx.beans.property.Property<Color> strokeColor) {
		try {
			FXMLLoader loader = new FXMLLoader(Interface.class.getResource("Mouvements.fxml"));
			AnchorPane rightAnchorPane = loader.load();
			MouvementsController mouvementsController = loader.getController();
			mouvementsController.initialize(figureController);
			mouvementsController.setColorPikers(fillColor, strokeColor);

			borderPane.setRight(rightAnchorPane);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void canvasResizeAble() {
		borderPane.heightProperty().addListener((observable, oldValue, newValue) -> {
			if(oldValue.doubleValue() != 0.0) {
				canvas.heightProperty().set(canvas.heightProperty().getValue() +
						newValue.doubleValue() - oldValue.doubleValue());
			}
		});

		borderPane.widthProperty().addListener((observable, oldValue, newValue) -> {
			if(oldValue.doubleValue() != 0.0) {
				canvas.widthProperty().set(canvas.widthProperty().getValue() +
						newValue.doubleValue() - oldValue.doubleValue());
			}
		});
	}

	public void setFigureAndStage(Figure figure, Stage stage) {
		displayShadowMenuItem.setSelected(true);
		displaySegmentsMenuItem.setSelected(true);
		displayFacesMenuItem.setSelected(true);

		displayShadowMenuItem.disableProperty().bind(displayFacesMenuItem.selectedProperty().not());

		FigureController figureController = new FigureController(figure);
		CanvasController canvasController =
				new CanvasController(canvas, figureController,
						displayShadowMenuItem.selectedProperty(),
						displaySegmentsMenuItem.selectedProperty(),
						displayFacesMenuItem.selectedProperty(),
						true);


		addMouvementsControlRightBorderPane(figureController, canvasController.getFillColor(), canvasController.getStrokeColor());
		centerMenuItem.setOnAction(event -> {
			figureController.centrage(canvas.getWidth(), canvas.getHeight());
		});

		propertiesTableView.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);

		ObservableList<Property> infos = FXCollections.observableArrayList();
		infos.add(new Property("Nom de la figure", figure.getInformations().getFigureName()));
		infos.add(new Property("Auteur", figure.getInformations().getAuthor()));
		infos.add(new Property("Description", figure.getInformations().getDescription()));
		infos.add(new Property("Date de creation", figure.getInformations().getCreationDate()));

		propertiesTableView.setItems(infos);
		propertiesTableView.setEditable(true);
		propertyNameTableColumn.setCellValueFactory(new PropertyValueFactory<>("propertyName"));
		propertyValueTableColumn.setCellValueFactory(new PropertyValueFactory<>("propertyValue"));
		propertyValueTableColumn.setEditable(true);
		propertyValueTableColumn.setOnEditCommit(event -> {
			String newValue = event.getNewValue();
			if(newValue != null)
				switch (event.getTablePosition().getRow()) {
					case 0: figure.getInformations().setFigureName(newValue); break;
					case 1: figure.getInformations().setAuthor(newValue); break;
					case 2: figure.getInformations().setDescription(newValue); break;
					case 4: figure.getInformations().setCreationDate(newValue); break;
				}
		});

		propertyValueTableColumn.setCellFactory(param -> new TextFieldTableCell<>(new StringConverter<>() {
			@Override
			public String toString(String textWroteByUser) {
				String textInCell = null;

				switch (propertiesTableView.getSelectionModel().selectedIndexProperty().get()) {
					case 0: textInCell = figure.getInformations().getFigureName(); break;
					case 1: textInCell = figure.getInformations().getAuthor(); break;
					case 2: textInCell = figure.getInformations().getDescription(); break;
					case 4: textInCell = figure.getInformations().getCreationDate(); break;
				}

				return textWroteByUser == null ? textInCell : textWroteByUser;
			}

			@Override
			public String fromString(String string) {
				return string;
			}
		}));

		saveAsMenuItem.setOnAction(event -> {
			FileChooser fileChooser = new FileChooser();
			fileChooser.setTitle("Save " + figure.getInformations().getFigureName());
			fileChooser.getExtensionFilters().add(new FileChooser.ExtensionFilter("PLY files (*.ply)", "*.ply"));
			File saveTo = fileChooser.showSaveDialog(stage);

			if(saveTo != null)
				new PlyWriter(figure).write(saveTo);
		});

		saveMenuItem.setOnAction(event -> {
			new PlyWriter(figure).write(figure.getInformations().getFile());
		});
	}
}
