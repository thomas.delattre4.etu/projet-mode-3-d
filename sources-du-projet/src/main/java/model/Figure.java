package model;

import java.util.*;

import maths.SimpleCalculations;

/**
 * Figure representee par un ensemble de polygones
 * @author adil.benameur.etu
 *
 */
public class Figure extends Observable {
	public final static Vertex SOURCE_ECLAIRAGE = new Vertex(1,1,1);
    protected FigureInfo informations;
    protected ArrayList<Polygon> polygons;
	protected ArrayList<Vertex> vertices;
    protected Double[] extremumValueVerticesX;
    protected Double[] extremumValueVerticesY;
    protected Double[] extremumValueVerticesZ;
	protected Vertex centreGravite;

	/**
	 * instancie une nouvelle figure vide
	 */
    public Figure() {
        this(new ArrayList<>());
    }

    /**
     * instancie une nouvelle figure avec des polygones définis
     * @param polygons liste de polygones qui representent la figure
     */
    public Figure(ArrayList<Polygon> polygons) {
        this.polygons = polygons;
        this.vertices = new ArrayList<Vertex>();
        this.informations = new FigureInfo();
    }

	/**
     * taille de la figure
     * @return le nombre de polygones
     */
	public int size() {
        return polygons.size();
    }
	
	/**
	 * recupere la longueur Y de la figure
	 * @return la longueur Y
	 */
    public double getFigureYLength() {
        Double[] val = getExtremumValueVerticesY();
        return val[1] - val[0];
    }

    /**
	 * recupere la longueur X de la figure
	 * @return la longueur X
	 */
    public double getFigureXLength() {
        Double[] val = getExtremumValueVerticesX();
        return val[1] - val[0];
    }
    
    /**
	 * recupere la longueur Z de la figure
	 * @return la longueur Z
	 */
    public double getFigureZLength() {
    	Double[] val = getExtremumValueVerticesZ();
        return val[1] - val[0];
    }

    /**
     * definit si, c'est le cas, le min et/ou le max d'un tableau par rapport a une valeur
     * @param minMax tableau qui sera verifie puis modifie
     * @param value valeur a verifier
     */
    private void setMinOrMax(Double[] minMax, double value) {
    	if(minMax[0] == null || minMax[0] > value) minMax[0] = value;
        if(minMax[1] == null || minMax[1] < value) minMax[1] = value;
    }

    /**
     * declare si la figure est vide
     * @return true si la figure n'est pas vide, false sinon
     */
    public boolean isBlank() {
        return polygons.isEmpty();
    }

    /**
     * boucle pour eviter la redondance, fait le tour de chaque valeur puis modifie le tableau min/max
     * @param values valeurs a parcourir
     * @param currentMinMax tableau a modifier
     */
    private void boucle(double[] values, Double[] currentMinMax) {
    	for(double value: values) {
    		this.setMinOrMax(currentMinMax, value);
    	}
    }

    /**
     * recharge les extremums de coordonnees X
     */
    private void reloadExtremumX() {
    	Double[] newValue = new Double[2];

		for(Polygon p: polygons) {
			this.boucle(p.getX(), newValue);
    	}

		this.extremumValueVerticesX = newValue;
    }

    /**
     * recharge les extremums de coordonnees Y
     */
    private void reloadExtremumY() {
    	Double[] newValue = new Double[2];

		for(Polygon p: polygons) {
			this.boucle(p.getY(), newValue);
    	}

		this.extremumValueVerticesY = newValue;
    }

    /**
     * recharge les extremums de coordonnees Z
     */
    private void reloadExtremumZ() {
    	Double[] newValue = new Double[2];

		for(Polygon p: polygons) {
			this.boucle(p.getZ(), newValue);
    	}

		this.extremumValueVerticesZ = newValue;
    }

    /**
     * calcule le centre de la figure
     * @return le sommet representant le centre de la figure
     */
    private Vertex reloadCentreGravite() {
    	double xValue = SimpleCalculations.average(this.getExtremumValueVerticesX());
		double yValue = SimpleCalculations.average(this.getExtremumValueVerticesY());
		double zValue = SimpleCalculations.average(this.getExtremumValueVerticesZ());
		return new Vertex(xValue, yValue, zValue);
    }

    public void updateCentreGravite() {
    	this.centreGravite = this.reloadCentreGravite();
    }


	public Double[] getExtremumValueVerticesX() {
		if(this.extremumValueVerticesX == null) this.reloadExtremumX();
		return this.extremumValueVerticesX;
    }

    public Double[] getExtremumValueVerticesY() {
    	if(this.extremumValueVerticesY == null) this.reloadExtremumY();
		return this.extremumValueVerticesY;
    }

    public Double[] getExtremumValueVerticesZ() {
    	if(this.extremumValueVerticesZ == null) this.reloadExtremumZ();
		return this.extremumValueVerticesZ;
    }
    
    public Vertex getCentreGravite() {
    	if(this.centreGravite == null) this.centreGravite = this.reloadCentreGravite();
    	return this.centreGravite;
    }
    
    public Vertex getTmpCentreGravite() {
    	return this.reloadCentreGravite();
    }
    
    
    
    public void update() {
    	this.reloadExtremumX();
    	this.reloadExtremumY();
    	this.reloadExtremumZ();
    	Collections.sort(this.polygons);
    }
    

    public void addPolygon(Polygon polygon) {
        polygons.add(polygon);
    }

    public ArrayList<Polygon> getPolygons() {
        return polygons;
    }
    
    @Override
    public String toString() {
        return "Figure{" +
                "informations=" + informations +
                ", polygons=" + polygons.size() +
                ", vertices=" + vertices.size() +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Figure figure = (Figure) o;
        return Objects.equals(polygons, figure.polygons);
    }

    @Override
    public int hashCode() {
        return Objects.hash(polygons);
    }


	public void setVertices(ArrayList<Vertex> vertices) {
		this.vertices.addAll(vertices);
	}
	
	public ArrayList<Vertex> getVertices() {
		return this.vertices;
	}
	
	public FigureInfo getInformations() {
		return this.informations;
	}
	
	public void setInformations(FigureInfo figureInfo) {
		this.informations = figureInfo;
	}

    public void changed() {
        update();
        setChanged();
        notifyObservers();
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public void finalize() throws Throwable {
        super.finalize();
    }
    
    public OmbreFigure createOmbre() {
    	return new OmbreFigure(this);
    }
}
