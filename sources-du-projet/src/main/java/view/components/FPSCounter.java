package view.components;

import javafx.animation.AnimationTimer;
import javafx.beans.property.StringProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;

public class FPSCounter extends Label {
    private final long[] frameTimes = new long[300];
    private int frameTimeIndex = 0 ;
    private boolean arrayFilled = false ;
    private StringProperty textProperty;

    public FPSCounter() {
        textProperty = this.textProperty();
        setStyle("-fx-background-color: white;");

        AnimationTimer frameRateMeter = new AnimationTimer() {
            @Override
            public void handle(long now) {
                long oldFrameTime = frameTimes[frameTimeIndex] ;
                frameTimes[frameTimeIndex] = now ;
                frameTimeIndex = (frameTimeIndex + 1) % frameTimes.length ;
                if (frameTimeIndex == 0) {
                    arrayFilled = true ;
                }
                if (arrayFilled) {
                    long elapsedNanos = now - oldFrameTime ;
                    long elapsedNanosPerFrame = elapsedNanos / frameTimes.length ;
                    double frameRate = 1_000_000_000.0 / elapsedNanosPerFrame ;

                    textProperty.setValue("FPS : " + (int) frameRate);
                }
            }
        };

        frameRateMeter.start();
    }
}
