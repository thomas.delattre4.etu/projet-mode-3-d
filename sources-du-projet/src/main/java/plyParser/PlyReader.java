package plyParser;

import java.io.*;
import java.util.Iterator;
import java.util.NoSuchElementException;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

/**
 * Classe permettant la lecture d'un fichier ply sous la forme d'un itérateur.
 */
public class PlyReader {
	private final File file;
	private PlyIterator fileIterator;
	private int lineNumber;
	private SimpleStringProperty comment;

	interface PlyIterator extends Iterator<String> {
		void reset();
	}

	public PlyReader(File file) throws FileNotFoundException {
		try {
			comment = new SimpleStringProperty();
			this.file = file;


			fileIterator = new PlyIterator() {
				private String nextLine = null;
				public int lineNumber;
				FileReader fileReader = new FileReader(file);
				BufferedReader bufferedReader = new BufferedReader(fileReader);

				@Override
				public void reset() {
					try {
						nextLine = null;
						lineNumber = 0;
						fileReader = new FileReader(file);
						bufferedReader = new BufferedReader(fileReader);
					} catch (java.io.FileNotFoundException e) {
						e.printStackTrace();
					}
				}

				@Override
				public boolean hasNext() {
					if (nextLine != null) {
						return true;
					} else {
						try {
							nextLine = this.bufferedReader.readLine();
							return (nextLine != null);
						} catch (IOException e) {
							throw new UncheckedIOException(e);
						}
					}
				}

				@Override
				public String next() {
					if (nextLine != null || hasNext()) {
						String line = nextLine;
						nextLine = null;
						lineNumber++;
						return line;
					} else {
						throw new NoSuchElementException();
					}
				}
			};
		} catch (java.io.FileNotFoundException e) {
			throw new FileNotFoundException("File not found : " + file.getAbsolutePath());
		}
	}

	/**
	 * Retourne la prochaine ligne dans le fichier.
	 * @return une ligne ligne du fichier
	 */
	public String getLine() {
		if(fileIterator.hasNext()) {
			String line = fileIterator.next();
			if(line.isBlank()) line = getLine();
			lineNumber++;
			return line;
		} else throw new WrongFileFormatException("Le fichier " + file.getPath() + " s'est fini de manière inattendu");
	}

	/**
	 * Retoune la prochaine ligne du fichier en excluant
	 * @return
	 */
	public String getLineWithoutComment() {
		String line = getLine();
		while (line.matches("comment.*")) {
			comment.set(line);
			line = getLine();
		}	
		return line; 
	}

	public boolean hasNext() {
		return fileIterator.hasNext();
	}

	/**
	 * Retourne le numéro de la ligne actuelle.
	 * @return le numéro de la ligne actuelle.
	 */
	public int lineNumber() {
		return lineNumber;
	}
	
	/**
	 * Retourne le commentaire actuel
	 * @return la string property du commentaire
	 */
	public SimpleStringProperty getComment() {
		return this.comment;
	}

    public void reset() {
		fileIterator.reset();
    }
}
