package plyParser;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import model.Figure;
import model.FigureInfo;
import model.Polygon;
import model.Vertex;
import util.BenchmarkHelper;

public class Parser {
    private int vertexCount;
    private int facesNumber;
    private ArrayList<String> propertiesList;
    private ArrayList<Vertex> vertices;
    private FigureInfo informations;
    private Figure figure;

    private PlyReader pR;
    private ParserHelper parserHelper;
    private ValidationHelper validationHelper;
    private Validate validate;
    private Find find;

    public Parser(File file) {
        figure = new Figure();
        propertiesList = new ArrayList<>();
        vertices = new ArrayList<>();

        pR = new PlyReader(file);
        pR.getComment().addListener(this.handleComment());
        validationHelper = new ValidationHelper(pR);
        parserHelper = new ParserHelper(pR);
        validate = new Validate(pR,  validationHelper);
        find = new Find(pR, parserHelper);
        informations = new FigureInfo();
        informations.setFile(file);
    }

    /**
     * cree un change listener pour gerer les commentaire
     * @return un change listener dont le  "changed" permet d'ajouter une information si new value est un commentaire accepte
     */
    private ChangeListener<? super String> handleComment() {
    	return new ChangeListener<String>() {

			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				if(!find.auteur(newValue).equals("")) informations.setAuthor(find.auteur(newValue).trim());
				if(!find.dateCreation(newValue).equals("")) informations.setCreationDate(find.dateCreation(newValue).trim());
				if(!find.description(newValue).equals("")) informations.setDescription(find.description(newValue).trim());
				if(!find.nom(newValue).equals("")) informations.setFigureName(find.nom(newValue).trim());
			}
    	};
    }

	/**
     * Analyse et vérifie le fichier.
     */
    void parseFile() {
        try {
            parseHeader();
            parseVertex();
            parseFaces();
        } catch (Exception e) {
            throw WrongFileFormatException.errorAtLine(pR.lineNumber());
        }
        reset();
    }

    /**
     * Valide l'entête du ply et assigne le nombre de vertex et de faces et les noms des propriétés définit dans le fichier.
     */
    void parseHeader() {
        validate.ply();
        validate.format();
        vertexCount = Integer.parseInt(find.vertexElement(validate.vertexElement()));

        String line = pR.getLineWithoutComment();
        while(validate.isProperty(line)) {
            String propertyName = find.propertyName(validate.property(line));
            if(propertiesList.contains(propertyName))
                throw new WrongFileFormatException("Property " + propertyName + " is already defined");
            propertiesList.add(propertyName);
            line = pR.getLineWithoutComment();
        }
        facesNumber = Integer.parseInt(find.elementFaces(validate.elementFaces(line)));
        validate.propertyList();
        validate.endHeader();

        figure.setInformations(informations);
    }

    /**
     * A cause du caractère itératif du parser il doit être reset après chaque lecture dans le fichier ply
     * La fonction reset permet de remettre à zéro le lecteur du ply, la liste de propriété (x,y,z...) et la liste des vertex
     */
    private void reset() {
        pR.reset();
        propertiesList.clear();
        vertices.clear();
    }

    /**
     * Valide les vertex du ply et assigne un tableau de vertex
     */
    void parseVertex() {
        int propertyNumber = propertiesList.size()-1;

        for (int vertexIdx = 0; vertexIdx < vertexCount; vertexIdx++) {
            HashMap<String, Double> otherProperties = new HashMap<>();
            ArrayList<String> propertiesValues = find.vertices(validate.vertice(propertyNumber));
            double x=0,y=0,z=0;

            for (int propertyIdx = 0; propertyIdx < propertiesValues.size(); propertyIdx++) {
                double propertyValue = parserHelper.parseDouble(propertiesValues.get(propertyIdx));
                if (propertiesList.get(propertyIdx).equals("x")) x = propertyValue;
                else if (propertiesList.get(propertyIdx).equals("y")) y = propertyValue;
                else if (propertiesList.get(propertyIdx).equals("z")) z = propertyValue;
                else
                    otherProperties.put(propertiesList.get(propertyIdx), parserHelper.parseDouble(propertiesValues.get(propertyIdx)));
            }

            vertices.add(new Vertex(x,y,z,otherProperties));
        }
        figure.setVertices(vertices);
    }

    /**
     * Valide les faces (polygon) du ply et assigne un tableau de face
     */
    void parseFaces() {
        for (int faceIdx = 0; faceIdx < facesNumber; faceIdx++) {
            String line = pR.getLine();
            int numberOfVertex = parserHelper.parseInt(find.verticesNumber(line));

            ArrayList<String> vertexNumbers = find.faces(validate.faces(line, numberOfVertex));
            ArrayList<Vertex> faceVertices = new ArrayList<>();
            for (String vertexNumber :
                    vertexNumbers) {
                faceVertices.add(vertices.get(parserHelper.parseInt(vertexNumber)));
            }

            figure.addPolygon(new Polygon(faceVertices));
        }
    }

    /**
     * Retourne la la figure du fichier ply passé au constructeur.
     * Le fichier est analysé et la figure définit si elle est vide
     * @return la figure du fichier ply passé au constructeur
     */
    public Figure getFigure() {
        parseFile();
        return figure;
    }

    public FigureInfo getInformations() {
        parseHeader();
        reset();
        return informations;
    }

    /**
     * Retourne l'ensemble des sommets necessaires a la figure
     * @return une liste de sommets
     */
    public ArrayList<Vertex> getVertices() {
    	return this.vertices;
    }
}
